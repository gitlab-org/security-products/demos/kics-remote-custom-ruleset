# kics-remote-custom-ruleset

This project illustrates how to configure the `kics` SAST scanner with a custom remote ruleset.

The `main.tf` file has a known Terraform vulnerability.

The remote ruleset located in this project, https://gitlab.com/gitlab-org/security-products/demos/kics-remote-ruleset,  disables this vulnerability.